## [0.2.0] StarBow ⭐️🎀 - 2020-02-16

## Added
- Score emojis
- SQLite
- Sick Ascii
- Rest Api for question data

## Changed
- Better code split for commands
- Scores stored in database
- Questions stored in database

## Removed
- Fileparser
- Question data Text Files
- Panier command (useless and too powerfull)

## Fixed
- Removed var

## [0.1.1] SunSax ☀️🎷 - 2020-02-04

## Added
- Bot Activity with version
- Monospace answers

## Changed
- Improved getHint method clarity

## Fixed
- Class exports
- Bad data in datafile

## Removed
- Global channel storage
- Pause and resume because it's never used

## [0.1.0] ShitPickle 💩🥒 - 2020-01-26

### Added
- Normalize function to clean input strings
- Version command
- Changelog command
- Question topic can now be used: `!irkbot start [topic]`
- Changelog
- Prettier config file
- Test database file

### Changed
- Response delay improved
- New Question delay 15s to 10s
- Improved fileparser

### Fixed
- Global indentation and syntax
- Bot self answer globally disabled
- Question can now be set to null

### Removed
- Bad questions from database file

### Broke
- addQ command needs to take topic into account