export type Score = {
  id: string
  val: number
  idUser: string
  username: string
  game: 'Quizz' | 'MusicQuizz'
}
