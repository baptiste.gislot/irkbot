import { Message } from 'discord.js'

export type MessageExtended = Message & {
  command: string
  args: string[]
  rawArgs: string[]
}
