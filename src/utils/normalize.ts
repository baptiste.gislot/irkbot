export const Normalize = (input: string) => {
  return input
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
    .trim()
}

export const NormalizeMusicName = (input: string) => {
  const regex = new RegExp('([^)]*)')
  let sanitizedInput = input.match(regex)![0]
  let splited = sanitizedInput.split(' - ')
  if (splited.length > 1) {
    sanitizedInput = splited[0]
  }
  const match = sanitizedInput.match(
    new RegExp(`^[a-zA-Z\u00C0-\u00FF '&#,!?$.]*`)
  )
  if (match && match[0].length) {
    return Normalize(match[0])
  } else {
    throw Error('Error sanitizing input')
  }
}
