export class Tween {
  start: number
  end: number
  duration: number
  interval: number
  step: number = 0

  constructor(start: number, end: number, duration: number, interval: number) {
    this.start = start
    this.end = end
    this.duration = duration
    this.interval = interval
  }

  linear() {
    var c = this.end - this.start
    return (c * this.step) / this.duration + this.start
  }

  easeInQuad() {
    var c = this.end - this.start
    return c * (this.step /= this.duration) * this.step + this.start
  }

  easeOutQuad() {
    var c = this.end - this.start
    return -c * (this.step /= this.duration) * (this.step - 2) + this.start
  }

  go(tween: 'linear' | 'easeInQuad' | 'easeOutQuad', callback: Function) {
    this.step = 0
    const incrementTimer = setInterval(() => {
      callback(this[tween]())
      this.step += this.interval
    }, this.interval)

    setInterval(() => {
      clearInterval(incrementTimer)
    }, this.duration)
  }
}
