export class Timer {
  callback: () => void
  timerId: any
  delay: number
  remainingTime: number
  startTime: number

  constructor(callback: () => void, delay: number) {
    this.callback = callback
    this.timerId = this.delay = this.remainingTime = this.startTime = delay
  }

  setCallback(callback: () => void) {
    this.callback = callback
  }

  setDelay(delay: number) {
    this.delay = delay
  }

  start() {
    this.resume()
  }

  resume() {
    this.startTime = new Date().getTime()
    clearTimeout(this.timerId)
    this.timerId = setTimeout(this.callback, this.remainingTime)
  }

  pause() {
    clearTimeout(this.timerId)
    this.remainingTime -= new Date().getTime() - this.startTime
  }

  stop(): number {
    clearTimeout(this.timerId)
    return new Date().getTime() - this.startTime
  }
}
