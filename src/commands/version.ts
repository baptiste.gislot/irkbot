import { botVersion } from '../../data/utils'
import { MessageExtended } from '../models/MessageExtended'

export const version = (message: MessageExtended) => {
  message.channel.send(`Irkbot version: ${botVersion}`)
}
