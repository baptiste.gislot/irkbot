import axios from 'axios'
import { MessageExtended } from '../models/MessageExtended'

const apiKey = 'a07f994635e34d67214c576625182a19'

export const meteo = (message: MessageExtended) => {
  const location = message.args.join(' ')
  return axios
    .get(
      `https://api.openweathermap.org/data/2.5/weather?q=${location}&units=metric&appid=${apiKey}&lang=fr`
    )
    .then((res) => {
      const response =
        `Le temps à ${res.data.name} est actuellment ${res.data.weather[0].description} ` +
        `avec une température actuelle de ${res.data.main.temp}°C et un ressenti de ${res.data.main.feels_like}°C.`
      message.channel.send(response)
    })
}
