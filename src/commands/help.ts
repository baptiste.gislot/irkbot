import fs from 'fs'
import path from 'path'
import { MessageExtended } from '../models/MessageExtended'

export const help = (message: MessageExtended) => {
  const helpPath = path.normalize(
    path.join(__dirname, '../../data/helpCommands.md')
  )
  const help = fs.readFileSync(helpPath)
  message.channel.send(
    `\`\`\`Markdown
` + `${help.toString()}\`\`\``
  )
}
