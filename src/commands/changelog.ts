import fs from 'fs'
import path from 'path'
import { MessageExtended } from '../models/MessageExtended'

export const changelog = (message: MessageExtended) => {
  const changelogPath = path.normalize(
    path.join(__dirname, '/../../changelog.md')
  )
  const changelog = fs.readFileSync(changelogPath)
  message.channel.send(
    `\`\`\`Markdown
` + `${changelog.toString()}\`\`\``
  )
}
