// https://developer.spotify.com/documentation/general/guides/authorization/app-settings/
export const clientId: string = 'SpotifyClientId'
export const clientSecret: string = 'SpotifyClientSecret'
