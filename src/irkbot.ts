import { Message } from 'discord.js'
import { botVersion } from '../data/utils'
import { changelog } from './commands/changelog'
import { help } from './commands/help'
import { meteo } from './commands/meteo'
import { version } from './commands/version'
import { Discord } from './discord'
import { GameManager } from './games/GameManager'
import { Insult } from './insult'
import { MessageExtended } from './models/MessageExtended'
import { Normalize } from './utils/normalize'

export class Irkbot {
  insult: Insult
  gameManager: GameManager

  constructor() {
    this.insult = new Insult()
    this.gameManager = new GameManager()
  }

  /**
   * Runs the bot
   * @param {*} token
   */
  run(token: string): void {
    const bot = new Discord(token)
    bot.client.on('ready', () => {
      console.log(`Irkbot V${botVersion} online`)
      bot.client.user?.setActivity(`V${botVersion}`, { type: 'PLAYING' })
    })

    // Listen to upcoming messages
    bot.client.on('message', (message: Message) => {
      // Dont answer to yourself
      if (message.author.username != bot.client.user?.username) {
        if (message.content.startsWith('!irk')) {
          this.handleCmd(message as MessageExtended)
        } else {
          this.handleMessage(message as MessageExtended)
        }
      }
    })
  }

  /**
   * Handles a message
   * @param {*} message
   */
  handleMessage(message: MessageExtended): void {
    if (this.gameManager.handleMessage(message)) {
      return
    } else if (message.content.includes('irkbot')) {
      message.channel.send(this.insult.constructRandomMessage())
    }
  }

  /**
   * Handles a command message
   * @param {*} message
   */
  async handleCmd(message: MessageExtended): Promise<void> {
    const normalizedMsgSplit = Normalize(message.content).split(' ')
    message.command = normalizedMsgSplit[1]
    message.args = normalizedMsgSplit.slice(2)
    message.rawArgs = message.content.split(' ').slice(2)

    if (await this.gameManager.handleCommand(message)) {
      return
    } else if (message.command === 'meteo') {
      meteo(message)
      return
    } else if (message.command === 'changelog') {
      changelog(message)
      return
    } else if (message.command === 'version') {
      version(message)
      return
    } else if (message.command === 'help') {
      help(message)
      return
    } else {
      message.react('🤔')
    }
  }
}
