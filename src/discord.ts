import { Client, Intents } from 'discord.js'

export const Discord = class {
  client: Client

  constructor(token: string) {
    this.client = new Client({
      intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_VOICE_STATES,
      ],
    })
    this.client.login(token)
  }
}
