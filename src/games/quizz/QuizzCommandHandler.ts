import { Quizz } from '.'
import { MessageExtended } from '../../models/MessageExtended'
import { GameManager } from '../GameManager'

export const QuizzCommandHandler = async (
  message: MessageExtended,
  gameManager: GameManager
) => {
  const game = gameManager.getGame('quizz', message) as Quizz
  if (game) {
    if (game.running && message.command === 'stop') {
      game.stop()
      return true
    } else if (game.running && message.command === 'pass') {
      game.passQuestion()
      return true
    } else if (message.command === 'addq') {
      game.addQuestion(message.content)
      return true
    }
  }
  if (message.command === 'score' || message.command === 'scores') {
    const msg = await game.scoreManager.getScores(
      message,
      'quizz',
      message.args[1]
    )
    message.channel.send(msg)
    return true
  } else if (message.command === 'reset') {
    game.scoreManager.resetScores()
    message.channel.send('Score du jour remis à zéro')
    return true
  } else {
    if (message.command === 'start') {
      gameManager.initQuizz(message).start(message.args[1])
      return true
    }
  }

  return false
}
