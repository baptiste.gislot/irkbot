export type Question = {
  id: number
  topic: string
  question: string
  answer: string
}
