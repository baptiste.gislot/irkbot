import { insert, selectOne } from '../../../db'
import { Question } from './models/Question'

export class QuizzQuestionManager {
  question?: Question
  hiddenResponse: string = ''
  hideChar: string = '⬝'

  /**
   * Gets a random question from the db
   */
  async getRandom(topic: string) {
    // for sql LIKE syntax
    const topicLike = `%${topic}%`
    this.question = await selectOne(
      { sub: 'games/quizz', file: 'randomQuestionByTopic' },
      [topicLike]
    )
  }

  /**
   * Checks if topic is available
   */
  async checkTopic(topic: string) {
    // for sql LIKE syntax
    const topicLike = `%${topic}%`
    return (this.question = await selectOne(
      { sub: 'games/quizz', file: 'questionsCountByTopic' },
      [topicLike]
    ))
  }

  /**
   * Return a hint
   * @param {*} level
   */
  getHint(level: number) {
    // Hide all letters
    if (level === 0) {
      this.hiddenResponse = this.question!.answer.replace(
        /[a-zA-ZÀ-Ÿ0-9]/g,
        this.hideChar
      )
    }
    // Reveal only first letter of each words
    else if (level === 1) {
      const words = this.question!.answer.split(' ')
      this.hiddenResponse = words
        .map((word: string) => {
          if (word.length == 1) {
            return this.hideChar
          } else {
            // Extract first char and replace any char for the rest
            const rest = word.slice(1)
            return (
              word.charAt(0) + rest.replace(/[a-zA-ZÀ-Ÿ0-9]/g, this.hideChar)
            )
          }
        })
        .join(' ')
    }
    // Reveal chars but not all
    else if (level > 1) {
      const words = this.question!.answer.split(' ')
      const hdnWords = this.hiddenResponse.split(' ')
      for (const idx in words) {
        // If one letter, no reveal
        if (words[+idx].length !== 1) {
          // Reveal (lenWord/level+1) letters, prevents full reveal
          const numLetterToShow = Math.floor(words[+idx].length / (level + 1))
          for (let k = 0; k < numLetterToShow; k++) {
            let isHiddenChar = false
            let rdmPosition = 0
            let securityCnt = 0
            // Try to find a random unrevealed letter
            while (!isHiddenChar) {
              rdmPosition = Math.floor(Math.random() * hdnWords[+idx].length)
              isHiddenChar = hdnWords[+idx][rdmPosition] === this.hideChar
              securityCnt++
              if (securityCnt > 50) {
                isHiddenChar = true
              } // Prevents infinite loop
            }
            // Rebuild word with added revealed letter
            hdnWords[+idx] =
              hdnWords[+idx].slice(0, rdmPosition) +
              words[+idx][rdmPosition] +
              hdnWords[+idx].slice(rdmPosition + 1)
          }
        }
      }
      this.hiddenResponse = hdnWords.join(' ')
    }
    return this.hiddenResponse
  }

  /**
   * Adds a question \ answer to database
   */
  async addQuestion(topic: string, question: string, answer: string) {
    return await insert({ sub: 'games/quizz', file: 'data' }, [
      topic,
      question,
      answer,
    ])
  }
}
