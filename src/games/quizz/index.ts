import { Message, TextChannel } from 'discord.js'
import { Normalize } from '../../utils/normalize'
import { Timer } from '../../utils/timer'
import { ScoreManager } from '../ScoreManager'
import { QuizzQuestionManager } from './QuizzQuestionManager'

export class Quizz {
  questionManager: QuizzQuestionManager
  channel: TextChannel
  scoreManager: ScoreManager
  hintTimer: Timer = new Timer(() => {}, 0)
  running: boolean = false
  topic: string = ''
  level: number = 0

  constructor(channel: TextChannel) {
    this.questionManager = new QuizzQuestionManager()
    this.channel = channel
    this.scoreManager = new ScoreManager()
  }

  /**
   * Returns answer in lowercase without accents
   */
  getAnswer() {
    return Normalize(this.questionManager.question!.answer)
  }

  /**
   * Starts the game
   */
  async start(topic: string) {
    topic = topic ? topic : ''
    // Returns number of questions associated to this topic
    const nbq = await this.questionManager.checkTopic(topic)
    if (topic == '') {
      this.running = true
      this.topic = topic
      this.channel.send(
        `Aucun Topic selectionné, nombre questions: ${nbq.count}`
      )
      this.newQuestion()
    } else if (nbq.count) {
      this.running = true
      this.topic = topic
      this.channel.send(
        `Topic selectionné: ${topic}, nombre questions associées: ${nbq.count}`
      )
      this.newQuestion()
    } else {
      this.channel.send('Topic inexistant')
    }
  }

  /**
   * Stops the game
   */
  stop() {
    this.running = false
    this.questionManager.question = undefined
    this.hintTimer.stop()
    this.channel.send('On arrête là !')
  }

  /**
   *
   * @param {*} delay
   */
  newDelayedQuestion(delay: number) {
    this.channel.send('Nouvelle question dans ' + delay + ' secondes.')
    this.hintTimer = new Timer(async () => {
      await this.newQuestion()
    }, delay * 1000)
    this.hintTimer.start()
  }
  /**
   * Post new question to channel
   */
  async newQuestion() {
    await this.questionManager.getRandom(this.topic)
    this.level = -1
    this.channel.send(
      `${this.questionManager.question?.topic} | ${this.questionManager.question?.question}`
    )
    this.hint()
  }

  /**
   * Post hint
   */
  hint() {
    this.level++
    this.channel.send(
      `==== Indice ${
        this.level + 1
      } ==== Pts: ${this.scoreManager.getPtsFromLevel(this.level)}  ====`
    )
    this.channel.send('`' + this.questionManager.getHint(this.level) + '`')
    this.hintTimer = new Timer(
      () => {
        if (this.level === 3) {
          this.displayResponse()
        } else {
          this.hint()
        }
      },
      4500 +
        (this.level === 0
          ? this.questionManager.question!.question.length * 25
          : 0) + // Add timeout for first level
        this.questionManager.question!.answer.length * 135
    )
    this.hintTimer.start()
  }

  /**
   *
   * @param {*} message sent message
   */
  responseFound(message: Message) {
    const pts = this.scoreManager.getPtsFromLevel(this.level)
    this.hintTimer.stop()
    this.channel.send(
      `Bravo ${message.author.username} la réponse était: ${this.questionManager.question?.answer}.`
    )
    this.questionManager.question = undefined
    const score = this.scoreManager.addScore(message, pts, 'quizz')
    this.channel.send(
      `${message.author.username} ${score.pts} pts ! ${
        score.streakScore ? `Avec ${score.streakScore} points bonus` : ''
      }`
    )
    if (this.scoreManager.streakCount > 1) {
      this.channel.send(
        `${message.author.username} est en 🔥 ! ${this.scoreManager.streakCount} bonne réponses d'affilé.`
      )
    }
    this.newDelayedQuestion(10)
    this.hintTimer.start()
  }

  /**
   * Displays response when no one found it
   */
  displayResponse() {
    this.channel.send('Super les mecs ...')
    this.channel.send(
      `La réponse était: ${this.questionManager.question?.answer}`
    )
    this.questionManager.question = undefined
    this.newDelayedQuestion(10)
  }

  /**
   *
   */
  passQuestion() {
    this.hintTimer.stop()
    this.channel.send(
      `Trop dur pour vous ?? La réponse était: ${this.questionManager.question?.answer}.`
    )
    this.questionManager.question = undefined
    this.newDelayedQuestion(10)
  }

  /**
   *
   * @param {*} command
   */
  addQuestion(command: string) {
    const cleanMsg = command.replace('!irkbot addq', '')
    const sCleanMsg = cleanMsg.split('|')
    try {
      this.questionManager.addQuestion(
        sCleanMsg[0].trim(),
        sCleanMsg[1].trim(),
        sCleanMsg[2].trim()
      )
      this.channel.send('Question ajoutée:')
      this.channel.send('q: ' + sCleanMsg[1].trim())
      this.channel.send('r: ' + sCleanMsg[2].trim())
    } catch (error) {
      this.channel.send('Mauvaise syntaxe.')
      this.channel.send('!irkbot addq question | response')
    }
  }
}
