import { Quizz } from '.'
import { MessageExtended } from '../../models/MessageExtended'
import { Normalize } from '../../utils/normalize'
import { GameManager } from '../GameManager'

export const QuizzMessageHandler = (
  message: MessageExtended,
  gameManager: GameManager
) => {
  const normalizedMsg = Normalize(message.content)
  const game = gameManager.getGame('quizz', message) as Quizz
  if (game && game.running && game.questionManager.question != null) {
    if (normalizedMsg === game.getAnswer()) {
      game.responseFound(message)
      return true
    }
  }

  return false
}
