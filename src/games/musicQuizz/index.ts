import {
  AudioPlayer,
  createAudioPlayer,
  createAudioResource,
  joinVoiceChannel,
  VoiceConnection,
} from '@discordjs/voice'
import { Message, MessageEmbed, TextChannel, VoiceState } from 'discord.js'
import { clientId, clientSecret } from '../../config'
import { MessageExtended } from '../../models/MessageExtended'
import { Timer } from '../../utils/timer'
import { ScoreManager } from '../ScoreManager'
import { Playlist } from './models/Playlist'
import { SpotifyApi } from './spotifyApi'
import { TrackManager } from './TrackManager'

export class MusicQuizz {
  channel: TextChannel
  running: boolean = false
  waitingForPlaylistChoice: boolean = false
  playlistName: string = ''
  playlists: Partial<Playlist>[] = []
  level: number = 0
  musicManager: TrackManager
  hintTimer: Timer = new Timer(() => {}, 0)
  player?: AudioPlayer
  connection?: VoiceConnection
  spotifyApi?: SpotifyApi
  voice?: VoiceState
  scoreManager: ScoreManager
  responseFoundTime: number = 0

  constructor(channel: TextChannel) {
    this.channel = channel
    this.musicManager = new TrackManager()
    this.scoreManager = new ScoreManager()
  }

  /**
   * Starts the game
   */
  async start(message: MessageExtended, playlistName: string) {
    if (!playlistName) {
      this.channel.send('Me faut une playlist ptit malin')
      return
    }

    this.spotifyApi = new SpotifyApi()
    await this.spotifyApi.init(clientId, clientSecret)

    this.voice = message.member?.voice

    if (!this.voice || !this.voice.channelId) {
      message.reply(
        'Tu dois rejoindre un serveur vocal avant de me parler comme ça...'
      )
      return
    }

    try {
      this.playlists = await this.spotifyApi.searchPlaylists(playlistName, {
        limit: 5,
        offset: 0,
      })
    } catch (error: any) {
      this.channel.send(`Mince y'a un soucis la ...`)
      console.log(error.message)
    }

    if (this.playlists.length > 1) {
      this.channel.send('Choisis ta playlist:')
      const embed = new MessageEmbed().setTitle('Choix de playlist')
      this.playlists.forEach((pl: Partial<Playlist>, idx: number) => {
        embed.addField(`${idx}`, pl.name!)
      })
      this.channel.send({ embeds: [embed] })
      this.waitingForPlaylistChoice = true
    } else {
      this.start2(this.playlists[0].id!)
    }
    this.running = true
  }

  async start2(playlistId: string) {
    try {
      this.musicManager.playlist = await this.spotifyApi!.getPlaylist(
        playlistId
      )
    } catch (error: any) {
      this.channel.send(
        `Arf c'est balo j'arrive pas a choper l'id de ta playlist :/`
      )
      console.log(error.message)
    }
    this.player = createAudioPlayer()
    if (!this.voice) {
      this.channel.send('Faut pas quitter le serveur vocal banane')
    } else {
      this.connection = joinVoiceChannel({
        channelId: this.voice?.channelId || '',
        guildId: this.voice?.guild.id || '',
        adapterCreator: this.voice!.guild.voiceAdapterCreator,
      })
      this.connection.subscribe(this.player)
      this.waitingForPlaylistChoice = false
      this.channel.send(
        `C'est parti ! ${this.musicManager.playlist!.name} - ${
          this.musicManager.playlist!.tracks.length
        } chansons.`
      )
      this.newMusic()
    }
  }

  /**
   * Stops the game
   */
  stop() {
    this.running = false
    this.musicManager.stop()
    this.hintTimer.stop()
    this.player!.stop()
    this.connection!.destroy()
    this.channel.send('On stop tout !')
  }

  /**
   *
   * @param {*} delay
   */
  newDelayedQuestion(delay: number) {
    this.channel.send(`Nouvelle musique dans ${delay} secondes.`)
    this.hintTimer = new Timer(async () => {
      await this.newMusic()
      this.responseFoundTime = 0
    }, delay * 1000)
    this.hintTimer.start()
  }

  /**
   * Post new question to channel
   */
  async newMusic() {
    try {
      this.musicManager.getRandom()
    } catch {
      this.channel.send(`Playlist terminée`)
      this.stop()
      return
    }
    this.level = -1
    let resource = createAudioResource(
      this.musicManager.currentMusic!.preview_url
    )
    this.channel.send(
      `Nouvelle musique ! Il en reste ${
        this.musicManager.playlist!.tracks.length
      }`
    )
    new Timer(async () => {
      this.player!.play(resource)
      this.hint()
    }, 500).start()
  }

  /**
   * Post hint
   */
  hint() {
    this.level++
    this.channel.send(
      `==== Indice ${
        this.level + 1
      } ==== Pts: ${this.scoreManager.getPtsFromLevel(this.level)}  ====`
    )
    this.channel.send('`' + this.musicManager.getHint(this.level) + '`')
    this.hintTimer = new Timer(() => {
      if (this.level === 3) {
        this.displayResponse()
      } else {
        this.hint()
      }
    }, 5500 + (this.level === 0 ? this.musicManager.currentMusic!.name.length * 135 : 0))
    this.hintTimer.start()
  }

  /**
   *
   * @param {*} message sent message
   */
  responseFound(message: Message) {
    if (this.responseFoundTime) {
      const timeDiff = (new Date().getTime() - this.responseFoundTime) / 1000
      this.channel.send(
        `Ah bah c'est con t'avais la bonne reponse mais t'es en retard de ${timeDiff.toFixed(
          3
        )}s`
      )
      return
    } else {
      this.responseFoundTime = new Date().getTime()
    }
    const pts = this.scoreManager.getPtsFromLevel(this.level)
    this.hintTimer.stop()
    this.channel.send(
      `Bravo ${message.author.username} la musique recherchée était: ${
        this.musicManager.currentMusic!.name
      } de ${this.musicManager
        .currentMusic!.artists.map((a) => a.name)
        .join(', ')}`
    )
    const score = this.scoreManager.addScore(message, pts, 'musicQuizz')
    this.channel.send(
      `${message.author.username} ${score.pts} pts ! ${
        score.streakScore ? `Avec ${score.streakScore} points bonus` : ''
      }`
    )
    if (this.scoreManager.streakCount > 1) {
      this.channel.send(
        `${message.author.username} est en 🔥 ! ${this.scoreManager.streakCount} bonne réponses d'affilé.`
      )
    }
    this.newDelayedQuestion(10)
    this.hintTimer.start()
  }

  /**
   * Displays response when no one found it
   */
  displayResponse() {
    this.channel.send('Ah bah bravo la culture...')
    this.channel.send(
      `La réponse était: ${
        this.musicManager.currentMusic!.name
      } de ${this.musicManager
        .currentMusic!.artists.map((a) => a.name)
        .join(', ')}`
    )
    this.musicManager.currentMusic = undefined
    this.newDelayedQuestion(10)
  }

  /**
   * Passes the question and asks another one
   */
  passQuestion() {
    this.hintTimer.stop()
    this.player!.stop()
    this.channel.send(
      `Ah bah je comprend moi aussi je trouve qu'elle est chier cette chanson... La réponse était: ${
        this.musicManager.currentMusic!.name
      }`
    )
    this.musicManager.currentMusic = undefined
    this.newDelayedQuestion(10)
  }
}
