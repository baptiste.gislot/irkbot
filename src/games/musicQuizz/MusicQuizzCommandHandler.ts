import { MusicQuizz } from '.'
import { MessageExtended } from '../../models/MessageExtended'
import { GameManager } from '../GameManager'

export const MusicQuizzCommandHandler = async (
  message: MessageExtended,
  gameManager: GameManager
) => {
  const game = gameManager.getGame('musicQuizz', message) as MusicQuizz
  if (game) {
    if (game.running && message.command === 'stop') {
      game.stop()
      return true
    } else if (game.running && message.command === 'pass') {
      game.passQuestion()
      return true
    }
  }
  if (message.command === 'score' || message.command === 'scores') {
    const msg = await game.scoreManager.getScores(
      message,
      'musicQuizz',
      message.args[1]
    )
    message.channel.send(msg)
    return true
  } else if (message.command === 'reset') {
    game.scoreManager.resetScores()
    message.channel.send('Score du jour remis à zéro')
    return true
  } else {
    if (message.command === 'start') {
      gameManager
        .initMusicQuizz(message)
        .start(message, message.rawArgs.slice(1).join(' '))
      return true
    }
  }

  return false
}
