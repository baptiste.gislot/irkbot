export type Track = {
  album: {
    album_type: string
    artists: { id: string; name: string }[]
    href: string
    id: string
    images: { url: string; height: number; width: number }[]
    name: string
    type: string
    uri: string
  }
  artists: { id: string; name: string }[]
  external_urls: {
    spotify: string
  }
  id: string
  name: string
  preview_url: string
  uri: string
}
