import { Track } from './Track'

export type Playlist = {
  description: string
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  images: [
    {
      height: number
      url: string
      width: number
    }
  ]
  name: string
  owner: {
    display_name: string
    external_urls: { spotify: string }
    href: string
    id: string
    type: string
    uri: string
  }
  public: null
  tracks: Track[]
}
