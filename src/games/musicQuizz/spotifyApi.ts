import { Playlist } from './models/Playlist'
import { Track } from './models/Track'

const SpotifyWebApi = require('spotify-web-api-node')

export class SpotifyApi {
  spotifyWebApi?: any

  async init(clientId: string, clientSecret: string): Promise<void> {
    // Create the api object with the credentials
    this.spotifyWebApi = new SpotifyWebApi({
      clientId: clientId,
      clientSecret: clientSecret,
    })

    // Retrieve an access token.
    const credentialsData = await this.spotifyWebApi.clientCredentialsGrant()
    this.spotifyWebApi.setAccessToken(credentialsData.body['access_token'])
  }

  async searchPlaylists(
    query: string,
    options: { limit: number; offset: number },
    callback?: undefined
  ): Promise<Partial<Playlist>[]> {
    const playlistSearch = this.parseQuery(query)
    if (playlistSearch.type === 'text') {
      try {
        const search = await this.spotifyWebApi.search(
          playlistSearch.query,
          ['playlist'],
          options,
          callback
        )
        return search.body.playlists.items
      } catch (error) {
        throw error
      }
    } else {
      return new Promise((resolve) => {
        resolve([{ id: playlistSearch.query } as Partial<Playlist>])
      })
    }
  }

  /**
   *
   * @param query
   * @returns
   */
  parseQuery(query: string) {
    if (query.startsWith('http')) {
      const regex = new RegExp(
        '^(https://open.spotify.com/user/spotify/playlist/|spotify:user:spotify:playlist:|https://open.spotify.com/playlist/)?([a-zA-Z0-9]+)(.*)$'
      )
      const match = query.match(regex)
      if (match) {
        return { type: 'id', query: match[2] }
      } else {
        throw Error
      }
    } else {
      // test for white spaces and length
      if (!/\s/g.test(query) && query.length === 22) {
        return { type: 'id', query }
      } else {
        return { type: 'text', query }
      }
    }
  }

  async getPlaylist(id: string) {
    const playlist = (await this.spotifyWebApi.getPlaylist(id)).body

    // if there is more tracks than the limit (100 by default)
    if (playlist.tracks.total > playlist.tracks.limit) {
      // Divide the total number of track by the limit to get the number of API calls
      for (
        let i = 1;
        i < Math.ceil(playlist.tracks.total / playlist.tracks.limit);
        i++
      ) {
        const trackToAdd = (
          await this.spotifyWebApi.getPlaylistTracks(id, {
            offset: playlist.tracks.limit * i, // Offset each call by the limit * the call's index
          })
        ).body

        // Push the retreived tracks into the array
        trackToAdd.items.forEach((item: any) =>
          playlist.tracks.items.push(item)
        )
      }
    }
    return {
      ...playlist,
      tracks: playlist.tracks.items
        .filter((i: any) => i.track.preview_url != null)
        .map((m: any) => m.track as Track),
    } as Playlist
  }
}
