import { Normalize, NormalizeMusicName } from '../../utils/normalize'
import { Playlist } from './models/Playlist'
import { Track } from './models/Track'

export class TrackManager {
  currentMusic?: Track
  playlist?: Playlist
  hiddenNameResponse: string = ''
  hiddenArtistResponse: string = ''
  hideChar: string = '⬝'

  /**
   * Gets a random Music from the db
   */
  getRandom() {
    if (!this.playlist!.tracks?.length) {
      throw Error('playlist empty')
    }
    const index = Math.floor(Math.random() * this.playlist!.tracks!.length)
    this.currentMusic = this.playlist!.tracks![index]
    this.playlist!.tracks!.splice(index, 1)

    try {
      this.currentMusic.name = NormalizeMusicName(this.currentMusic.name)
      this.currentMusic.artists = this.currentMusic.artists.map((a) => {
        return {
          id: a.id,
          name: Normalize(a.name),
        }
      })
      this.currentMusic.album.name = Normalize(this.currentMusic.album.name)
    } catch (error) {
      this.getRandom()
    }
  }

  /**
   * Gets a random Music from the db
   */
  async stop() {
    this.currentMusic = undefined
    this.playlist = undefined
  }

  /**
   * Return a hint
   * @param {*} level
   */
  getHint(level: number) {
    // Hide all letters
    if (level === 0) {
      this.hiddenNameResponse = this.currentMusic!.name.replace(
        /[a-zA-ZÀ-Ÿ0-9]/g,
        this.hideChar
      )
      this.hiddenArtistResponse = this.currentMusic!.artists.map((a) => a.name)
        .join(', ')
        .replace(/[a-zA-ZÀ-Ÿ0-9]/g, this.hideChar)
    }
    // Reveal only first letter of each words
    else if (level === 1) {
      const words = this.currentMusic!.name.split(' ')
      this.hiddenNameResponse = words
        .map((word: string) => {
          if (word.length == 1) {
            return this.hideChar
          } else {
            // Extract first char and replace any char for the rest
            const rest = word.slice(1)
            return (
              word.charAt(0) + rest.replace(/[a-zA-ZÀ-Ÿ0-9]/g, this.hideChar)
            )
          }
        })
        .join(' ')
    }
    // Reveal chars but not all
    else if (level > 1) {
      const words = this.currentMusic!.name.split(' ')
      const hdnWords = this.hiddenNameResponse.split(' ')
      for (const idx in words) {
        // If one letter, no reveal
        if (words[+idx].length !== 1) {
          // Reveal (lenWord/level+1) letters, prevents full reveal
          const numLetterToShow = Math.floor(words[+idx].length / (level + 1))
          for (let k = 0; k < numLetterToShow; k++) {
            let isHiddenChar = false
            let rdmPosition = 0
            let securityCnt = 0
            // Try to find a random unrevealed letter
            while (!isHiddenChar) {
              rdmPosition = Math.floor(Math.random() * hdnWords[+idx].length)
              isHiddenChar = hdnWords[+idx][rdmPosition] === this.hideChar
              securityCnt++
              if (securityCnt > 50) {
                isHiddenChar = true
              } // Prevents infinite loop
            }
            // Rebuild word with added revealed letter
            hdnWords[+idx] =
              hdnWords[+idx].slice(0, rdmPosition) +
              words[+idx][rdmPosition] +
              hdnWords[+idx].slice(rdmPosition + 1)
          }
        }
      }
      this.hiddenNameResponse = hdnWords.join(' ')
    }
    return `Titre: ${this.hiddenNameResponse} | Artiste${
      this.currentMusic!.artists.length > 1 ? 's (trouver un)' : ''
    }: ${this.hiddenArtistResponse}`
  }
}
