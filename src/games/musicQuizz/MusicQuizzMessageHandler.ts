import { MusicQuizz } from '.'
import { MessageExtended } from '../../models/MessageExtended'
import { Normalize } from '../../utils/normalize'
import { GameManager } from '../GameManager'

export const MusicQuizzMessageHandler = (
  message: MessageExtended,
  gameManager: GameManager
) => {
  const normalizedMsg = Normalize(message.content)
  const game = gameManager.getGame('musicQuizz', message) as MusicQuizz

  if (game && game.running) {
    if (game.waitingForPlaylistChoice) {
      game.start2(game.playlists[+normalizedMsg].id!)
    } else if (
      game.musicManager.currentMusic != null &&
      (normalizedMsg === game.musicManager.currentMusic.name ||
        game.musicManager.currentMusic.artists
          .map((a) => a.name)
          .includes(normalizedMsg))
    ) {
      game.responseFound(message)
      return true
    }
  }

  return false
}
