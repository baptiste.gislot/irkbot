import { Message, TextChannel } from 'discord.js'
import { GameType } from '../models/Game'
import { Score } from '../models/Score'

const db = require('../../db')

export class ScoreManager {
  currentStreakerId: string = ''
  streakCount: number = 0

  /**
   * Add score to scores
   * @param {*} message
   * @param {*} level
   */
  addScore = (message: Message, pts: number, gameType: GameType) => {
    if (this.currentStreakerId !== message.author.id) {
      this.currentStreakerId = message.author.id
      this.streakCount = 1
    } else {
      this.streakCount++
    }
    const streakScore = this.getPtsFromStreak()
    this.addScoreDB(message, gameType, pts + streakScore)
    return { pts, streakScore }
  }

  /**
   * Reset score for the today
   */
  resetScores = () => {
    throw 'todo'
  }

  /**
   * Get score for today
   * @param {*} dayScore
   */
  getScores = async (message: Message, gameType: GameType, date?: string) => {
    const dateParsed = date ? new Date(date) : new Date()
    const scores = await this.selectDayScore(message, dateParsed, gameType)
    if (!scores) {
      return 'No score for today'
    } else {
      const scoresWithName = scores.map((score: Score) => {
        // extract member from channel members with userid
        const channel = message.channel as TextChannel
        const member = channel.members
          .filter((member) => {
            return member.user.id == score.idUser
          })
          .values() // result is a map
          .next().value
        if (member) {
          return { username: member.user.username, val: score.val }
        } else {
          return { username: 'Unknown', val: score.val }
        }
      })
      // sort by val and get it ready to print
      const result = scoresWithName
        .sort((a: Score, b: Score) => {
          return b.val - a.val
        })
        .map((score: Score, idx: number) => {
          const medal =
            idx == 0 ? '🥇' : idx == 1 ? '🥈' : idx == 2 ? '🥉' : '💩'
          return `${medal} ${score.username}: ${score.val}pts`
        })
      return result.join(' | ')
    }
  }

  /**
   * Level to points
   * @param {*} level
   */
  getPtsFromLevel = (level: number) => {
    const ptsArray = [10, 7, 3, 1]
    try {
      return ptsArray[level]
    } catch (error) {
      return 0
    }
  }

  /**
   * Streak to points
   * @param {*} streak
   */
  getPtsFromStreak = () => {
    return Math.floor(this.streakCount / 2)
  }

  addScoreDB = async (message: Message, gameType: GameType, score: number) => {
    const scoreData: Score = await this.selectDayUserScore(message, gameType)
    if (scoreData) {
      score = scoreData.val + score
      this.updateScore(scoreData.id, score)
    } else {
      this.insertScore(message, score, gameType)
    }
  }

  selectDayUserScore = async (message: Message, gameType: GameType) => {
    return await db.selectOne({ sub: `games/score`, file: 'scoreDayUser' }, [
      message.author.id,
      message.channel.id,
      gameType,
    ])
  }

  selectDayScore = async (message: Message, date: Date, gameType: GameType) => {
    return await db.select({ sub: `games/score`, file: 'scoreDay' }, [
      message.channel.id,
      gameType,
      db.formatDate(date),
      db.formatDate(date),
    ])
  }

  insertScore = async (message: Message, score: number, gameType: GameType) => {
    const date = new Date()
    return await db.insert({ sub: `games/score`, file: 'score' }, [
      message.author.id,
      message.channel.id,
      score,
      db.formatDate(date),
      gameType,
    ])
  }

  updateScore = async (rowId: string, score: number) => {
    return await db.update({ sub: `games/score`, file: 'score' }, [
      score,
      rowId,
    ])
  }
}
