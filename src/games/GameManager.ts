import { Message, TextChannel } from 'discord.js'
import { GameType } from '../models/Game'
import { MessageExtended } from '../models/MessageExtended'
import { MusicQuizz } from './musicQuizz'
import { MusicQuizzCommandHandler } from './musicQuizz/MusicQuizzCommandHandler'
import { MusicQuizzMessageHandler } from './musicQuizz/MusicQuizzMessageHandler'
import { Quizz } from './quizz'
import { QuizzCommandHandler } from './quizz/QuizzCommandHandler'
import { QuizzMessageHandler } from './quizz/QuizzMessageHandler'

export class GameManager {
  games: any

  constructor() {
    this.games = {}
  }

  isGameRunning(gameType: GameType, message: Message) {
    return (
      !!this.games[message.channel.id] &&
      !!this.games[message.channel.id][gameType] &&
      this.games[message.channel.id][gameType].running
    )
  }

  getGame(gameType: GameType, message: Message) {
    if (this.games[message.channel.id]) {
      return this.games[message.channel.id][gameType]
    } else {
      return false
    }
  }

  /**
   * Handle commands for games, if command is matched > return to prevent further actions
   * @param {message} message
   */
  async handleCommand(message: MessageExtended) {
    if (
      message.args.find((a) => a === 'musicquizz') ||
      this.isGameRunning('musicQuizz', message)
    ) {
      return MusicQuizzCommandHandler(message, this)
    } else if (
      message.args.find((a) => a === 'quizz') ||
      this.isGameRunning('quizz', message)
    ) {
      return QuizzCommandHandler(message, this)
    }
  }

  /**
   * Handle message for games
   * TODO: handle running game only
   * @param {message} message
   */
  handleMessage(message: MessageExtended) {
    if (this.isGameRunning('quizz', message)) {
      return QuizzMessageHandler(message, this)
    } else if (this.isGameRunning('musicQuizz', message)) {
      return MusicQuizzMessageHandler(message, this)
    }
  }

  // Games
  /**
   * Init games container for channel
   * @param {message} message
   */
  initGames(message: Message) {
    if (!this.games[message.channel.id]) {
      this.games[message.channel.id] = {}
    }
  }

  /**
   * Init quizz game with channel id
   * @param {*} message
   */
  initQuizz(message: Message) {
    this.initGames(message)
    return (this.games[message.channel.id]['quizz'] = new Quizz(
      message.channel as TextChannel
    ))
  }

  /**
   * Init audio quizz game with channel id
   * @param {*} message
   */
  initMusicQuizz(message: Message) {
    this.initGames(message)
    return (this.games[message.channel.id]['musicQuizz'] = new MusicQuizz(
      message.channel as TextChannel
    ))
  }
}
