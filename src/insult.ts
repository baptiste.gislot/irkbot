const fs = require('fs')

export class Insult {
  messages: any

  constructor() {
    this.messages = {}
  }

  /**
   * Constructs a message
   */
  constructRandomMessage() {
    if (!this.messages.hasOwnProperty('pt1')) {
      this.messages = this.fetch()
    }
    let insult = ''
    for (let index = 1; index < 13; index++) {
      const tmpinsult = this.getRandomElement('pt' + index)
      const dd =
        'aeiouy'.indexOf(tmpinsult.charAt(0).toLowerCase()) > -1 ? "d'" : 'de '
      if (index == 1) {
        insult += 'Espèce ' + dd
      } else if (index == 6) {
        insult += ', '
      } else if (index == 10) {
        insult += '. Va '
      } else if (index == 11) {
        insult += '. Signé : Ton '
      }
      insult += tmpinsult + ' '
    }
    insult += '.'
    return insult
  }

  getRandomElement(Elt: string) {
    if (!this.messages.hasOwnProperty('pt1')) {
      this.messages = this.fetch()
    }
    const ridx = Math.floor(Math.random() * this.messages[Elt].length)
    return this.messages[Elt][ridx]
  }

  /**
   * Fetch data from json data file
   */
  fetch() {
    try {
      const messageString = fs.readFileSync(__dirname + '/../data/message.json')
      return JSON.parse(messageString)
    } catch (e) {
      return []
    }
  }
}
