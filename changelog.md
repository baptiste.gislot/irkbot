# Changelog (2 last versions)

## [0.4.0] LightningSnowman ⚡⛄ - 2022-02-09

## Added
- MusicQuizz !
- Streak system for both quizz

## Changed
- Commands (!irkbot start <gameName>)
- Shortcut command (can use !irk instead of !irkbot)

## [0.3.0] AntiVax 🚫💉 - 2022-01-27

## Changed
- Migrated to Typescript

## Fixed
- Removed let
