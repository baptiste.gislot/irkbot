SELECT *
FROM quizz_scores
WHERE 
idChannel = ?
AND day BETWEEN datetime(?, 'start of day') AND datetime(?, 'start of day', '+1 day') 