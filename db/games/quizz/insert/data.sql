INSERT INTO quizz_data(topic, question, answer)
  VALUES(?, ?, ?)
  ON CONFLICT(question) DO UPDATE SET
    answer=excluded.answer,
    topic=excluded.topic;