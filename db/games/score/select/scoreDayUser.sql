SELECT *
FROM quizz_scores
WHERE idUser = ?
AND idChannel = ?
AND gameType = ?
AND day BETWEEN datetime('now', 'start of day') AND datetime('now', 'start of day', '+1 day') 