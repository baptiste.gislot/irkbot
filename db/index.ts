import fs from 'fs'
import path from 'path'
const sqlite3 = require('sqlite3').verbose()

type SQLPath = {
  sub: string
  file: string
}

// Connect to sqlite
export const db = new sqlite3.Database(
  path.normalize(path.join(__dirname, '../db/irkbot.db')),
  sqlite3.OPEN_READWRITE,
  (err: Error) => {
    if (err) {
      return console.error(err.message)
    }
    console.log('Connected to the irkbot.db SQlite database.')
  }
)

// Create Tables
const createFiles = fs
  .readdirSync(path.normalize(path.join(__dirname, '../db/create')))
  .filter((file: string) => file.endsWith('.sql'))

db.serialize(function () {
  for (const file of createFiles) {
    const create = fs
      .readFileSync(
        path.normalize(path.join(__dirname, `../db/create/${file}`))
      )
      .toString()

    db.run(create)
  }
})

// Functions
/**
 * Format a js date to sqlite format
 * @param {Date} date
 */
export const formatDate = (date: Date) => {
  return date.toISOString().replace('T', ' ').replace('Z', '')
}

/**
 * Select one row
 * @param {SQLPath} sqlPath
 * @param {Array} args
 */
export const selectOne = (sqlPath: SQLPath, args: string[]): Promise<any> => {
  return new Promise(function (resolve, reject) {
    const sqlSelect = fs
      .readFileSync(
        path.normalize(
          path.join(
            __dirname,
            `../db/${sqlPath.sub}/select/${sqlPath.file}.sql`
          )
        )
      )
      .toString()

    db.get(sqlSelect, args, (err: Error, row: any) => {
      if (err) {
        throw reject(err)
      } else {
        resolve(row)
      }
    })
  })
}

/**
 * Select many rows
 * @param {Object} sqlPath
 * @param {Array} args
 */
export const select = async (sqlPath: SQLPath, args?: string[]) => {
  return new Promise(function (resolve, reject) {
    const sqlFile = fs
      .readFileSync(
        path.normalize(
          path.join(
            __dirname,
            `../db/${sqlPath.sub}/select/${sqlPath.file}.sql`
          )
        )
      )
      .toString()

    db.all(sqlFile, args, (err: Error, rows: any) => {
      if (err) {
        throw reject(err)
      } else {
        resolve(rows)
      }
    })
  })
}

export const insert = async (sqlPath: SQLPath, args: any) => {
  return await run('insert', sqlPath, args)
}

export const update = async (sqlPath: SQLPath, args: string[]) => {
  return await run('update', sqlPath, args)
}

export const remove = async (sqlPath: SQLPath, args: string[]) => {
  return await run('remove', sqlPath, args)
}

/**
 * Run sql operation
 * @param {String} op
 * @param {Object} sqlPath
 * @param {Array} args
 */
const run = (op: string, sqlPath: SQLPath, args: string[]) => {
  return new Promise(function (resolve, reject) {
    const sqlFile = fs
      .readFileSync(
        path.normalize(
          path.join(__dirname, `../db/${sqlPath.sub}/${op}/${sqlPath.file}.sql`)
        )
      )
      .toString()

    db.run(sqlFile, args, function (err: Error, rows: any) {
      if (err) {
        throw reject(err)
      } else {
        resolve(rows)
      }
    })
  })
}
