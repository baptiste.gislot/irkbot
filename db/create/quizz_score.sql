CREATE TABLE IF NOT EXISTS quizz_scores (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  idUser INTEGER,
  idChannel INTEGER,
  val INTEGER,
  day TEXT,
  gameType TEXT
  );