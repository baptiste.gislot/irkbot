CREATE TABLE IF NOT EXISTS quizz_data (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  topic TEXT,
  question TEXT UNIQUE,
  answer TEXT);