import { botVersion } from './data/utils'
import { Irkbot } from './src/irkbot'

const yargs = require('yargs')

// Read command args
const argv = yargs
  .command('run', 'Run the bot', {
    token: {
      describe: 'Discord bot token',
      demand: false,
      alias: 't',
      default: '',
    },
    api: {
      describe: 'run api',
      demand: false,
      alias: 'api',
      default: '',
    },
  })
  .help().argv

function runBot() {
  const { generateDependencyReport } = require('@discordjs/voice')
  console.log(generateDependencyReport())
  const irkbot = new Irkbot()
  irkbot.run(argv.token)
}

function runApi() {
  require('./api')
}

/**
 * Noice
 */
function printAscii() {
  console.log(` ___  ________  ___  __    ________  ________  _________    `)
  console.log(
    `|\\  \\|\\   __  \\|\\  \\|\\  \\ |\\   __  \\|\\   __  \\|\\___   ___\\  `
  )
  console.log(
    `\\ \\  \\ \\  \\|\\  \\ \\  \\/  /|\\ \\  \\|\\ /\\ \\  \\|\\  \\|___ \\  \\_|  `
  )
  console.log(
    ` \\ \\  \\ \\   _  _\\ \\   ___  \\ \\   __  \\ \\  \\\\\\  \\   \\ \\  \\   `
  )
  console.log(
    `  \\ \\  \\ \\  \\\\  \\\\ \\  \\\\ \\  \\ \\  \\|\\  \\ \\  \\\\\\  \\   \\ \\  \\  `
  )
  console.log(
    `   \\ \\__\\ \\__\\\\ _\\\\ \\__\\\\ \\__\\ \\_______\\ \\_______\\   \\ \\__\\ `
  )
  console.log(
    `    \\|__|\\|__|\\|__|\\|__| \\|__|\\|_______|\\|_______|    \\|__| `
  )
  console.log(
    `                                                             Version ${botVersion}`
  )
}

const opt = argv._[0]

// Action selection
if (opt === 'run') {
  printAscii()
  runBot()
  if (argv.api !== undefined && ['', 'TRUE', 'true'].includes(argv.api)) {
    runApi()
  }
} else {
  console.log('Command not recognized')
  process.abort()
}
