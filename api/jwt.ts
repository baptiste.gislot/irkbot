import { NextFunction, Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { key } from './config'

// Middleware used to check if a token is valid and still living

export const checkToken = (req: Request, res: Response, next: NextFunction) => {
  // Firstly checking if the token is given
  if (req.headers.authorization) {
    // Get rid of the 'Bearer' string and keeping only the token
    const token = req.headers.authorization.split(' ')[1]
    // Verifying it and sending appropriate result
    jwt.verify(token, key, (err: any) => {
      if (err) {
        res.status(403).send('Bad token')
      }
      return next()
    })
  } else {
    res.status(403).send('Not logged in')
  }
}
