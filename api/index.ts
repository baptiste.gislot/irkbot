import bcrypt from 'bcrypt'
import bodyParser from 'body-parser'
import cors from 'cors'
import express, { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { insert, remove, select, selectOne, update } from '../db'
import { key } from './config'
import { checkToken } from './jwt'

const app = express()
app.use(express.json())
app.use(express.urlencoded())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(bodyParser.json())
const saltRounds = 10
const port = 3001

app.get('/login', checkToken, (_req: Request, res: Response) => {
  // We check if the token is valid and up to date via the checkToken middleware
  res.status(200).send(true)
})

app.post('/login', async (req: Request, res: Response) => {
  // We create a token with an expiration date of 24h
  const user = { name: req.body.username, exp: Date.now() + 24 * 60 * 60 }
  const accessToken = jwt.sign(user, key)

  // Selecting the requested user in the db to compare password
  const data = await selectOne({ sub: 'users', file: 'user' }, [
    req.body.username,
  ])

  if (data) {
    // If a user is found we compare the passwords
    bcrypt.compare(req.body.password, data.password, (_err, result) => {
      if (result) {
        // If the passwords match we send the token to the front
        res.status(200).send(accessToken)
      } else {
        res.status(403).send('Wrong password')
      }
    })
  } else {
    res.status(401).send("Username / Password don't match")
  }
})

app.get('/users', async (_req: Request, res: Response) => {
  // We get all users from the BD
  const data = await select({ sub: 'users', file: 'users' }, [])
  if (data) {
    res.status(200).send(data)
  } else {
    res.sendStatus(404)
  }
})

app.get('/user/:username', async (req: Request, res: Response) => {
  // We get a specific user from the DB by it's username
  const data = await selectOne({ sub: 'users', file: 'user' }, [
    req.params.username,
  ])
  if (data) {
    res.status(200).send(data)
  } else {
    res.status(404).send('No user found')
  }
})

app.patch('/users/:username', checkToken, (req: Request, res: Response) => {
  // We check if a new password is provided in order to update it
  if (req.body.password) {
    // If a new password is provided we hash and salt it before storing it in the BD
    bcrypt.hash(req.body.password, saltRounds, async (_err, hash) => {
      const data = await update({ sub: 'users', file: 'user' }, [
        hash,
        req.params.username,
      ])
      res
        .status(201)
        .send(`${req.params.username}'s password has been modified.`)
    })
  } else {
    res.status(400).send('No new password provided')
  }
})

app.delete(
  '/users/:username',
  checkToken,
  async (req: Request, res: Response) => {
    // Deleting a specific user from the DB by it's username
    const data = remove({ sub: 'users', file: 'user' }, [req.params.username])
    if (!data) {
      res.status(200).send(`User ${req.params.username} deleted with success`)
    } else {
      res.status(404).send('Something went wrong')
    }
  }
)

app.post('/users', (req: Request, res: Response) => {
  // We check if we have a username and a password to register
  if (req.body.username && req.body.password) {
    // Salting and Hashing the password before storing it in the DB
    bcrypt.hash(req.body.password, saltRounds, async (_err, hash) => {
      // Storing the username and salted/hashed password in the DB
      const data = await insert({ sub: 'users', file: 'user' }, [
        req.body.username,
        hash,
      ])
      res.status(201).send(`User added with ${data}`)
    })
  } else {
    res.status(404).send('Missing username or password')
  }
})

app.get('/questions/count', async (_req: Request, res: Response) => {
  // We count the number of questions that are in the DB
  const data = await select({ sub: 'games/quizz', file: 'countQuestions' })
  if (data) {
    res.status(200).send(data)
  } else {
    res.status(400).send('An error occured')
  }
})

app.get('/questions/random', async (_req: Request, res: Response) => {
  // Selecting one random question from the DB and sending it
  const data = await selectOne(
    { sub: 'games/quizz', file: 'randomQuestionByTopic' },
    [`%%`]
  )
  res.status(200).send(data)
})

app.get('/questions/:id', async (req: Request, res: Response) => {
  // Selecting a specific question by the id provided in the URI and sending it
  const data = await select({ sub: 'games/quizz', file: 'questionById' }, [
    req.params.id,
  ])
  res.status(200).send(data)
})

app.get('/questions', async (req: Request, res: Response) => {
  let data
  // If we have a specified start and gap we select in the DB only the number of questions asked
  if (req.query.start && req.query.gap) {
    data = await select({ sub: 'games/quizz', file: 'allQuestionsPagined' }, [
      req.query.start as string,
      req.query.gap as string,
    ])
  } else {
    // If not we select and send all questions storred in the DB
    data = await select({ sub: 'games/quizz', file: 'allQuestions' }, [])
  }

  res.status(200).json(data)
})

app.post('/questions', checkToken, async (req: Request, res: Response) => {
  // We check if we have all the info needed to store a new question in the DB
  if (req.body.topic && req.body.question && req.body.answer) {
    // We store it in the DB and send it's new ID
    const data = await insert({ sub: 'games/quizz', file: 'data' }, [
      req.body.topic,
      req.body.question,
      req.body.answer,
    ])
    res.status(201).send(`Question added with id: ${data}`)
  } else {
    res.status(400).send(`Malformed body`)
  }
})

app.patch('/questions/:id', checkToken, async (req: Request, res: Response) => {
  // We check if we have all the info needed to edit a specific question from the DB
  if (req.body.topic && req.body.question && req.body.answer) {
    // Editing it and sending result
    await update({ sub: 'games/quizz', file: 'data' }, [
      req.body.topic,
      req.body.question,
      req.body.answer,
      req.params.id,
    ])
    res.status(201).send(`Question with id: ${req.params.id} updated`)
  } else {
    res.status(400).send(`Malformed body`)
  }
})

app.delete(
  '/questions/:id',
  checkToken,
  async (req: Request, res: Response) => {
    // Deleting from the app a specified question with the ID in the URI
    const data = await remove({ sub: 'games/quizz', file: 'data' }, [
      req.params.id,
    ])
    if (!data) {
      res
        .status(200)
        .send(`Question with id: ${req.params.id} deleted with success`)
    } else {
      res.status(404).send('Something went wrong')
    }
  }
)

app.listen(port, () => console.log(`Api listening on port ${port}`))
