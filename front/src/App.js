import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Login from './Components/Login'
import Dashboard from './Components/Dashboard'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  const [isLogged, setIsLogged] = useState(false)

  useEffect(() => {
    if (localStorage.getItem('token')) {
      axios
        .get('http://127.0.0.1:3001/login', {
          headers: {
            authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
        .then((res, err) => {
          if (res.data === true) {
            setIsLogged(true)
          }
        })
    } else {
    }
  }, [])

  return (
    <div className="App" style={{ margin: '5px 50px', maxWidth: '200vh' }}>
      {isLogged ? <Dashboard /> : <Login isLogged={setIsLogged} />}
    </div>
  )
}

export default App
