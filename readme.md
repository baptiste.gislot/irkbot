# IrkBot

## Build and run docker image
* `docker build <path to docker file>`
* `docker run <image id>`

## Run the bot

### How to

* `nvm install 16`
* `apt-get install autoconf automake g++ libtool ffmpeg`
* `cd irkbot/`
* `sudo apt-get install build-essential`   (makes sure you have the required tools)
* `sudo npm i`             (installs required node modules)
* `sudo npm install sqlite3 --build-from-source`   (build sqlite3 package for the OS it's running on)
* `ts-node app.ts run -t <token here>`  (discord)

### Options & flags

```none
run
  --token, -t         Bot token       str
parse
  --file, -f          File to parse   str
```

## Bot usage

### How to bot

When the bot has joined the room, type:

`!irkbot <command>`

### Commands

Look at data/helpCommands.md